﻿namespace FindStrGUI
{
    partial class Form_FindStrGUI
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            textBox_SearchString = new TextBox();
            checkBox_Regex = new CheckBox();
            checkBox_SkipBinaries = new CheckBox();
            checkBox_MatchCase = new CheckBox();
            button_SearchFiles = new Button();
            checkBox_Recursive = new CheckBox();
            folderBrowserDialog = new FolderBrowserDialog();
            textBox_FileFilter = new TextBox();
            button_FolderSelect = new Button();
            textBox_Folder = new TextBox();
            SuspendLayout();
            // 
            // textBox_SearchString
            // 
            textBox_SearchString.Location = new Point(12, 12);
            textBox_SearchString.Name = "textBox_SearchString";
            textBox_SearchString.PlaceholderText = "Find";
            textBox_SearchString.Size = new Size(360, 23);
            textBox_SearchString.TabIndex = 0;
            // 
            // checkBox_Regex
            // 
            checkBox_Regex.AutoSize = true;
            checkBox_Regex.Location = new Point(12, 66);
            checkBox_Regex.Name = "checkBox_Regex";
            checkBox_Regex.Size = new Size(58, 19);
            checkBox_Regex.TabIndex = 3;
            checkBox_Regex.Text = "Regex";
            checkBox_Regex.UseVisualStyleBackColor = true;
            // 
            // checkBox_SkipBinaries
            // 
            checkBox_SkipBinaries.AutoSize = true;
            checkBox_SkipBinaries.Checked = true;
            checkBox_SkipBinaries.CheckState = CheckState.Checked;
            checkBox_SkipBinaries.Location = new Point(121, 66);
            checkBox_SkipBinaries.Name = "checkBox_SkipBinaries";
            checkBox_SkipBinaries.Size = new Size(92, 19);
            checkBox_SkipBinaries.TabIndex = 4;
            checkBox_SkipBinaries.Text = "Skip Binaries";
            checkBox_SkipBinaries.UseVisualStyleBackColor = true;
            // 
            // checkBox_MatchCase
            // 
            checkBox_MatchCase.AutoSize = true;
            checkBox_MatchCase.Location = new Point(12, 41);
            checkBox_MatchCase.Name = "checkBox_MatchCase";
            checkBox_MatchCase.Size = new Size(88, 19);
            checkBox_MatchCase.TabIndex = 1;
            checkBox_MatchCase.Text = "Match Case";
            checkBox_MatchCase.UseVisualStyleBackColor = true;
            // 
            // button_SearchFiles
            // 
            button_SearchFiles.Location = new Point(253, 41);
            button_SearchFiles.Name = "button_SearchFiles";
            button_SearchFiles.Size = new Size(119, 23);
            button_SearchFiles.TabIndex = 8;
            button_SearchFiles.Text = "Search Files";
            button_SearchFiles.UseVisualStyleBackColor = true;
            button_SearchFiles.Click += button_SearchFiles_Click;
            // 
            // checkBox_Recursive
            // 
            checkBox_Recursive.AutoSize = true;
            checkBox_Recursive.Location = new Point(121, 41);
            checkBox_Recursive.Name = "checkBox_Recursive";
            checkBox_Recursive.Size = new Size(76, 19);
            checkBox_Recursive.TabIndex = 2;
            checkBox_Recursive.Text = "Recursive";
            checkBox_Recursive.UseVisualStyleBackColor = true;
            // 
            // textBox_FileFilter
            // 
            textBox_FileFilter.Location = new Point(12, 120);
            textBox_FileFilter.Name = "textBox_FileFilter";
            textBox_FileFilter.PlaceholderText = "File Filters";
            textBox_FileFilter.Size = new Size(360, 23);
            textBox_FileFilter.TabIndex = 7;
            // 
            // button_FolderSelect
            // 
            button_FolderSelect.Location = new Point(344, 91);
            button_FolderSelect.Name = "button_FolderSelect";
            button_FolderSelect.Size = new Size(28, 23);
            button_FolderSelect.TabIndex = 6;
            button_FolderSelect.Text = "...";
            button_FolderSelect.UseVisualStyleBackColor = true;
            button_FolderSelect.Click += button_FolderSelect_Click;
            // 
            // textBox_Folder
            // 
            textBox_Folder.Location = new Point(12, 91);
            textBox_Folder.Name = "textBox_Folder";
            textBox_Folder.PlaceholderText = "Folder";
            textBox_Folder.Size = new Size(326, 23);
            textBox_Folder.TabIndex = 5;
            // 
            // Form_FindStrGUI
            // 
            AcceptButton = button_SearchFiles;
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(384, 161);
            Controls.Add(textBox_Folder);
            Controls.Add(button_FolderSelect);
            Controls.Add(textBox_FileFilter);
            Controls.Add(checkBox_Recursive);
            Controls.Add(button_SearchFiles);
            Controls.Add(checkBox_MatchCase);
            Controls.Add(checkBox_SkipBinaries);
            Controls.Add(checkBox_Regex);
            Controls.Add(textBox_SearchString);
            MaximizeBox = false;
            Name = "Form_FindStrGUI";
            SizeGripStyle = SizeGripStyle.Hide;
            Text = "FindStrGUI";
            Load += Form1_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private TextBox textBox_SearchString;
        private CheckBox checkBox_Regex;
        private CheckBox checkBox_SkipBinaries;
        private CheckBox checkBox_MatchCase;
        private Button button_SearchFiles;
        private CheckBox checkBox_Recursive;
        private FolderBrowserDialog folderBrowserDialog;
        private TextBox textBox_FileFilter;
        private Button button_FolderSelect;
        private TextBox textBox_Folder;
    }
}
