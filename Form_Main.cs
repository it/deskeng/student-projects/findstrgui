using System.Web;

namespace FindStrGUI
{
    public partial class Form_FindStrGUI : Form
    {
        public Form_FindStrGUI()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox_Folder.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            textBox_Folder.AutoCompleteSource = AutoCompleteSource.FileSystem;
        }

        private void button_SearchFiles_Click(object sender, EventArgs e)
        {
            if (textBox_SearchString.Text.Trim().Equals(string.Empty))
            {
                MessageBox.Show("Search cannot be empty or only whitespace.", "Search String Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Directory.Exists(textBox_Folder.Text))
            {
                MessageBox.Show("Folder \"" + textBox_Folder.Text + "\" does not exist.", "Root Folder Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Form_FileSearchResults searchWindow = new Form_FileSearchResults(FormatArgString());
            searchWindow.Show();
        }

        private string FormatArgString()
        {
            string args = string.Empty;

            args += ((checkBox_MatchCase.Checked) ? "" : "/i ");
            args += ((checkBox_Recursive.Checked) ? "/s " : "");
            args += ((checkBox_Regex.Checked) ? "/r " : "");
            args += ((checkBox_SkipBinaries.Checked) ? "/p " : "");
            args += "/n ";  // Print line numbers
            
            args += "/c:\"" + textBox_SearchString.Text + "\" ";

            string fileFilter = string.Empty;
            if (textBox_FileFilter.Text.Equals(string.Empty))
            {
                fileFilter = "\"" + Path.Join(textBox_Folder.Text, "*") + "\"";
            } 
            else
            {
                string[] filters = textBox_FileFilter.Text.Split(',');
                foreach (string filter in filters)
                {
                    fileFilter += "\"" + Path.Join(textBox_Folder.Text, filter.Trim()) + "\" ";
                }
            }
            args += fileFilter;

            return args;
        }

        private void button_FolderSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
                textBox_Folder.Text = dialog.SelectedPath;
        }
    }
}
