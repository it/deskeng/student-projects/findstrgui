﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindStrGUI
{
    /// <summary>
    /// Represents the path, line_number, and value of a result from FindStr.
    /// Assumes that line numbers are enabled with /n
    /// </summary>
    public class Result
    {
        public string FilePath { get; set; }
        
        public string Line { get; set; }
        
        public string Content { get; set; }


        public Result(string result)
        {
            string[] tokens = result.Split(':');

            if (tokens.Length < 3)
            {
                Debug.WriteLine("Too few tokens in result, did you forget to enable line numbers?");
                FilePath = "Error resolving result";
                Line = "0";
                Content = result;
                return;
            }

            int index = 0;
            
            // Read path, retain drive letter
            if (result.Length > 1 && result[1] == ':')
            {
                FilePath = string.Join(":", tokens[index], tokens[index + 1]);
                index++;
            }
            else 
            { 
                FilePath = tokens[index];
            }    
            index++;
            FilePath = Path.GetFullPath(FilePath);

            // Read line number, easy
            Line = tokens[index++];

            int content_truncate_length = 500;

            // Read content, restoring colons that may have been in the file
            // Also loosely limits the length of the content field
            int length;
            if ((length = tokens[index].Length) < content_truncate_length)
            {
                Content = tokens[index++];
                content_truncate_length -= length;

                while (index < tokens.Length && content_truncate_length > 0)
                {
                    Content += (":" + tokens[index++]);
                    content_truncate_length -= length + 1;
                }
            }
            else
            {
                Content = tokens[index].Substring(0, content_truncate_length);
                content_truncate_length = 0;
            }
                
            if (content_truncate_length <= 0) 
                    Content += " ... (Truncated: See full in source file)";
        }
    }
}
