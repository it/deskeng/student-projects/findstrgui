﻿namespace FindStrGUI
{
    partial class Form_FileSearchResults
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            textBox_CMDCommand = new TextBox();
            statusStrip1 = new StatusStrip();
            toolStripProgressBar = new ToolStripProgressBar();
            toolStripStatusLabel_MatchText = new ToolStripStatusLabel();
            toolStripStatusLabel_MatchCount = new ToolStripStatusLabel();
            dataGridView_Results = new DataGridView();
            contextMenuStrip1 = new ContextMenuStrip(components);
            openInNotepadToolStripMenuItem = new ToolStripMenuItem();
            openFolderToolStripMenuItem = new ToolStripMenuItem();
            copyContentToolStripMenuItem = new ToolStripMenuItem();
            resultBindingSource = new BindingSource(components);
            button_KillSearch = new Button();
            filePathDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            lineDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            contentDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView_Results).BeginInit();
            contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)resultBindingSource).BeginInit();
            SuspendLayout();
            // 
            // textBox_CMDCommand
            // 
            textBox_CMDCommand.Dock = DockStyle.Top;
            textBox_CMDCommand.Location = new Point(0, 0);
            textBox_CMDCommand.Name = "textBox_CMDCommand";
            textBox_CMDCommand.ReadOnly = true;
            textBox_CMDCommand.Size = new Size(640, 23);
            textBox_CMDCommand.TabIndex = 1;
            textBox_CMDCommand.Text = "findstr /s /i test *";
            textBox_CMDCommand.WordWrap = false;
            // 
            // statusStrip1
            // 
            statusStrip1.Items.AddRange(new ToolStripItem[] { toolStripProgressBar, toolStripStatusLabel_MatchText, toolStripStatusLabel_MatchCount });
            statusStrip1.Location = new Point(0, 365);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(640, 22);
            statusStrip1.TabIndex = 2;
            statusStrip1.Text = "statusStrip1";
            // 
            // toolStripProgressBar
            // 
            toolStripProgressBar.Name = "toolStripProgressBar";
            toolStripProgressBar.Size = new Size(100, 16);
            // 
            // toolStripStatusLabel_MatchText
            // 
            toolStripStatusLabel_MatchText.Name = "toolStripStatusLabel_MatchText";
            toolStripStatusLabel_MatchText.Size = new Size(55, 17);
            toolStripStatusLabel_MatchText.Text = "Matches:";
            // 
            // toolStripStatusLabel_MatchCount
            // 
            toolStripStatusLabel_MatchCount.Name = "toolStripStatusLabel_MatchCount";
            toolStripStatusLabel_MatchCount.Size = new Size(13, 17);
            toolStripStatusLabel_MatchCount.Text = "0";
            // 
            // dataGridView_Results
            // 
            dataGridView_Results.AllowUserToAddRows = false;
            dataGridView_Results.AllowUserToDeleteRows = false;
            dataGridView_Results.AllowUserToOrderColumns = true;
            dataGridView_Results.AllowUserToResizeRows = false;
            dataGridView_Results.AutoGenerateColumns = false;
            dataGridView_Results.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridView_Results.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView_Results.Columns.AddRange(new DataGridViewColumn[] { filePathDataGridViewTextBoxColumn, lineDataGridViewTextBoxColumn, contentDataGridViewTextBoxColumn });
            dataGridView_Results.ContextMenuStrip = contextMenuStrip1;
            dataGridView_Results.DataSource = resultBindingSource;
            dataGridView_Results.Dock = DockStyle.Fill;
            dataGridView_Results.Location = new Point(0, 23);
            dataGridView_Results.Name = "dataGridView_Results";
            dataGridView_Results.ReadOnly = true;
            dataGridView_Results.RowHeadersVisible = false;
            dataGridView_Results.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView_Results.ShowEditingIcon = false;
            dataGridView_Results.Size = new Size(640, 342);
            dataGridView_Results.TabIndex = 3;
            // 
            // contextMenuStrip1
            // 
            contextMenuStrip1.Items.AddRange(new ToolStripItem[] { openInNotepadToolStripMenuItem, openFolderToolStripMenuItem, copyContentToolStripMenuItem });
            contextMenuStrip1.Name = "contextMenuStrip1";
            contextMenuStrip1.Size = new Size(166, 70);
            // 
            // openInNotepadToolStripMenuItem
            // 
            openInNotepadToolStripMenuItem.Name = "openInNotepadToolStripMenuItem";
            openInNotepadToolStripMenuItem.Size = new Size(165, 22);
            openInNotepadToolStripMenuItem.Text = "Open in Notepad";
            openInNotepadToolStripMenuItem.Click += openInNotepadToolStripMenuItem_Click;
            // 
            // openFolderToolStripMenuItem
            // 
            openFolderToolStripMenuItem.Name = "openFolderToolStripMenuItem";
            openFolderToolStripMenuItem.Size = new Size(165, 22);
            openFolderToolStripMenuItem.Text = "Open Folder";
            openFolderToolStripMenuItem.Click += openFolderToolStripMenuItem_Click;
            // 
            // copyContentToolStripMenuItem
            // 
            copyContentToolStripMenuItem.Name = "copyContentToolStripMenuItem";
            copyContentToolStripMenuItem.Size = new Size(165, 22);
            copyContentToolStripMenuItem.Text = "Copy Content";
            copyContentToolStripMenuItem.Click += copyContentToolStripMenuItem_Click;
            // 
            // resultBindingSource
            // 
            resultBindingSource.DataSource = typeof(Result);
            // 
            // button_KillSearch
            // 
            button_KillSearch.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            button_KillSearch.Location = new Point(542, 365);
            button_KillSearch.Name = "button_KillSearch";
            button_KillSearch.Size = new Size(75, 22);
            button_KillSearch.TabIndex = 4;
            button_KillSearch.Text = "Kill Search";
            button_KillSearch.UseVisualStyleBackColor = true;
            button_KillSearch.Click += button_KillSearch_Click;
            // 
            // filePathDataGridViewTextBoxColumn
            // 
            filePathDataGridViewTextBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            filePathDataGridViewTextBoxColumn.DataPropertyName = "FilePath";
            filePathDataGridViewTextBoxColumn.HeaderText = "Path";
            filePathDataGridViewTextBoxColumn.Name = "filePathDataGridViewTextBoxColumn";
            filePathDataGridViewTextBoxColumn.ReadOnly = true;
            filePathDataGridViewTextBoxColumn.Width = 56;
            // 
            // lineDataGridViewTextBoxColumn
            // 
            lineDataGridViewTextBoxColumn.DataPropertyName = "Line";
            lineDataGridViewTextBoxColumn.HeaderText = "Line";
            lineDataGridViewTextBoxColumn.Name = "lineDataGridViewTextBoxColumn";
            lineDataGridViewTextBoxColumn.ReadOnly = true;
            lineDataGridViewTextBoxColumn.Width = 54;
            // 
            // contentDataGridViewTextBoxColumn
            // 
            contentDataGridViewTextBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            contentDataGridViewTextBoxColumn.DataPropertyName = "Content";
            contentDataGridViewTextBoxColumn.HeaderText = "Content";
            contentDataGridViewTextBoxColumn.Name = "contentDataGridViewTextBoxColumn";
            contentDataGridViewTextBoxColumn.ReadOnly = true;
            contentDataGridViewTextBoxColumn.Width = 75;
            // 
            // Form_FileSearchResults
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(640, 387);
            Controls.Add(button_KillSearch);
            Controls.Add(dataGridView_Results);
            Controls.Add(statusStrip1);
            Controls.Add(textBox_CMDCommand);
            Name = "Form_FileSearchResults";
            Text = "File Search";
            Load += Form_FileSearchResults_Load;
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView_Results).EndInit();
            contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)resultBindingSource).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private TextBox textBox_CMDCommand;
        private StatusStrip statusStrip1;
        private ToolStripProgressBar toolStripProgressBar;
        private ToolStripStatusLabel toolStripStatusLabel_MatchText;
        private ToolStripStatusLabel toolStripStatusLabel_MatchCount;
        private DataGridView dataGridView_Results;
        private BindingSource resultBindingSource;
        private Button button_KillSearch;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem openInNotepadToolStripMenuItem;
        private ToolStripMenuItem openFolderToolStripMenuItem;
        private ToolStripMenuItem copyContentToolStripMenuItem;
        private DataGridViewTextBoxColumn filePathDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn lineDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn contentDataGridViewTextBoxColumn;
    }
}