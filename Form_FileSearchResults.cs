﻿using Microsoft.VisualBasic.Devices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FindStrGUI
{
    public partial class Form_FileSearchResults : Form
    {
        public BindingList<Result> ResultTable { get; private set; }

        const string FINDSTR_EXE = "findstr.exe";
        private readonly string arguments;
        private Process? proc_findstr;

        private DataGridViewRow? right_clicked_row;

        public Form_FileSearchResults(string arguments)
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(Form_FileSearchResults_FormClosing);

            this.arguments = arguments;
            textBox_CMDCommand.Text = "findstr " + arguments;

            ResultTable = new BindingList<Result>();
        }

        private void Form_FileSearchResults_Load(object? sender, EventArgs e)
        {
            InitDataGrid();
            StartFindstr();
        }

        void InitDataGrid()
        {
            ResultTable.RaiseListChangedEvents = true;
            ResultTable.AllowEdit = false;

            dataGridView_Results.DataSource = ResultTable;
            dataGridView_Results.RowContextMenuStripNeeded += new DataGridViewRowContextMenuStripNeededEventHandler(results_RowContextMenuStripNeeded);
        }

        Task? reader_task;
        CancellationTokenSource reader_canceler = new CancellationTokenSource();
        
        private void ReaderTask(CancellationToken ct)
        {
            if (ct.IsCancellationRequested) return;

            proc_findstr?.Start();
            string? resultLine;
            while ((resultLine = proc_findstr?.StandardOutput.ReadLine()) is not null)
            {
                if (ct.IsCancellationRequested) return;
                try
                {
                    Invoke(() =>
                    {
                        ResultTable.Add(new Result(resultLine));
                        toolStripStatusLabel_MatchCount.Text = ResultTable.Count.ToString();
                    });
                }
                catch (Exception ex) { Debug.WriteLine(ex.Message); }
            }

            if (ct.IsCancellationRequested) return;
            string? findstrError = proc_findstr?.StandardError.ReadToEnd();
            if (findstrError is not null && !findstrError.Equals(string.Empty))
                MessageBox.Show(findstrError, "FindStr Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            proc_findstr?.WaitForExit();
        }

        private async void StartFindstr()
        {
            try
            {
                proc_findstr = new Process() { StartInfo = new ProcessStartInfo() { FileName = FINDSTR_EXE, RedirectStandardOutput = true, RedirectStandardError = true, UseShellExecute = false, CreateNoWindow = true, Arguments = arguments } };
                toolStripProgressBar.Style = ProgressBarStyle.Marquee;

                reader_task = Task.Run(() => ReaderTask(reader_canceler.Token), reader_canceler.Token);
                await reader_task;

                toolStripProgressBar.Style = ProgressBarStyle.Continuous;
                toolStripProgressBar.Value = 100;
                button_KillSearch.Enabled = false;
            }
            catch (Exception ex) 
            {
                Debug.WriteLine(ex.Message);
            }
        }

        void Form_FileSearchResults_FormClosing(object? sender, FormClosingEventArgs e)
        {
            reader_canceler.Cancel();
            proc_findstr?.Kill();
        }

        private void button_KillSearch_Click(object? sender, EventArgs e)
        {
            proc_findstr?.Kill();
        }

        private void results_RowContextMenuStripNeeded(object? sender, DataGridViewRowContextMenuStripNeededEventArgs e)
        {
            // DataGridViewRow row = (sender as DataGridView).Rows[e.RowIndex];
            // if (row == null)
            //    return;
            if (sender is not null)
            {
                right_clicked_row = (sender as DataGridView)?.Rows[e.RowIndex];
            }  
            else
            {
                e.ContextMenuStrip?.Close();
            }
        }

        private void openInNotepadToolStripMenuItem_Click(object? sender, EventArgs e)
        {
            if (right_clicked_row is null)
            {
                MessageBox.Show("ERROR: right clicked object null.");
                return;
            }

            string? file_path = right_clicked_row?.Cells[0].Value.ToString();

            if (!File.Exists(file_path))
            {
                MessageBox.Show("The file does not exist.");
                return;
            }

            Process.Start("notepad.exe", file_path);
        }

        private void openFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (right_clicked_row == null)
            {
                MessageBox.Show("ERROR: right clicked object null.");
                return;
            }

            string? folder_path = Path.GetDirectoryName(right_clicked_row.Cells[0].Value.ToString());

            if (!Directory.Exists(folder_path))
            {
                MessageBox.Show("The folder does not exist.");
                return;
            }

            Process.Start("explorer.exe", folder_path);
        }

        private void copyContentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (right_clicked_row is null)
            {
                MessageBox.Show("ERROR: right clicked object null.");
                return;
            }

            string? text = right_clicked_row.Cells[2].Value.ToString();
            
            if (text is not null)
                Clipboard.SetText(text);
        }
    }
}

